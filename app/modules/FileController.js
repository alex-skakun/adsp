(function () {

    'use strict';


    var fileReader = require('file-reader'),
        chartController = require('chart-controller'),
        fileInput = document.getElementById('fileDialog'),
        welcome = document.getElementById('welcomeScreen'),
        chart = document.getElementById('chart'),
        rawData = document.getElementById('rawData'),
        loading = document.getElementById('loading'),
        fs = require('fs'),
        fileDescription = JSON.parse(fs.readFileSync('./resources/FileDescription.json').toString()),
        done = null;

    fileInput.addEventListener('change', function () {
        fileReader.read(this.value, fileDescription, done);
    }, false);

    global.fileController = {
        loading: function (state) {
            if (state) {
                loading.style.display = 'block';
            } else {
                loading.style.display = 'none';
            }
        },
        open: function () {
            fileInput.click();
            return {
                done: function (callback) {
                    done = callback;
                }
            };
        },
        showChart: function (data, reload) {
            chart.style.display = 'block';
            welcome.style.display = 'none';
            rawData.style.display = 'none';
            if (!chart.innerHTML.length || reload) {
                chartController.init(chart, chartController.getChartData(data));
            }
        },
        showData: function (data) {
            chart.style.display = 'none';
            rawData.style.display = 'block';
        }
    }


}());
